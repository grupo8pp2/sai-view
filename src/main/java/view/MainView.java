package view;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.JFrame;
import javax.swing.JTextField;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;



public class MainView {
	private JFrame _view;
	private JTextField panel;
	private JLabel lbl_portada;
	private JButton btnSaludame;
	private JRadioButton rdbtnSaludar;
	private JRadioButton rdbtnDespedirme;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainView window = new MainView();
					window._view.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		
	}
	
	public MainView() {
		inicializacion();
	}

	private void inicializacion() {
		_view = new JFrame();
		_view.setFont(new Font("Arial Black", Font.BOLD | Font.ITALIC, 15));
		_view.setResizable(false);
		_view.getContentPane().setBackground(Color.LIGHT_GRAY);
		_view.setTitle("Main");
		_view.setBounds(100, 100, 354, 325);
		_view.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		_view.getContentPane().setLayout(null);
		
		panel = new  JTextField();
		panel.setBounds(10, 44, 318, 74);
		panel.setFont(new Font("Verdana", Font.BOLD, 15));
		panel.setBackground(Color.WHITE);
		panel.setEditable(true);
		_view.getContentPane().add(panel);
		panel.setColumns(10);
		
		lbl_portada = new JLabel("Ingresa tu nombre:");
		lbl_portada.setFont(new Font("Tahoma", Font.BOLD, 13));
		lbl_portada.setBounds(10, 19, 135, 14);
		_view.getContentPane().add(lbl_portada);
		
		btnSaludame = new JButton("Realizar");
		btnSaludame.setBounds(120, 242, 109, 23);
		_view.getContentPane().add(btnSaludame);
		presionBtn(btnSaludame);
		
		rdbtnSaludar = new JRadioButton("Saludar");
		buttonGroup.add(rdbtnSaludar);
		rdbtnSaludar.setBounds(120, 135, 109, 23);
		_view.getContentPane().add(rdbtnSaludar);
		
		rdbtnDespedirme = new JRadioButton("Despedir");
		buttonGroup.add(rdbtnDespedirme);
		rdbtnDespedirme.setBounds(120, 173, 109, 23);
		_view.getContentPane().add(rdbtnDespedirme);
		
	}
	
	private void presionBtn(JButton btn) {
		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (validarProceso())
				{
					String seleccion = obtenerSeleccion();
					
				}
				else 
				{
					JOptionPane.showMessageDialog(null, "Verifique que todos los datos esenciales esten completos.");
				}
				
			}

			
		});
		
	}
	
	private String obtenerSeleccion() {
		String ret="";
		if (rdbtnSaludar.isSelected()) 
		{
			ret = rdbtnSaludar.getText();
		} 
		else 
		{
			ret = rdbtnDespedirme.getText();
		}
		
		return ret;
	}
	

	private boolean validarProceso() {
		return ( !panel.getText().isEmpty() && (rdbtnSaludar.isSelected() || rdbtnDespedirme.isSelected())) ;
		
	}
	
	
}
